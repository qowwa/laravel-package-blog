
<script src="{{asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    tinymce.init({
        selector: '#content',
        menubar: false,
        theme: 'modern',
        width: 500,
        height: 150
    });
</script>


@section('content')


    {!! Form::model($post,['method'=>'PATCH' , ['action'=>'Qowwa\Blog\PostsController@store',$post->id] , 'files'=> true ]) !!}

    {{ csrf_field() }}

        <div class="form-group">
            {!! Form::text('title' ,null , ['class'=>'form-control','placeholder'=>'Title']) !!}
        </div>

        <div class="form-group">
            {!! Form::textarea('body' ,null , ['class'=>'form-control','placeholder'=>'Content','id'=>'content']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('post_img' , 'Image:') !!}
            {!! Form::file('post_img' , ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::select('tag_id' ,$tags,null , ['class'=>'form-control','placeholder'=>'Select Tag']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('tags' , 'Or Add New Tags') !!}
            {!! Form::text('tag_name' ,null , ['class'=>'form-control','placeholder'=>'separated by comma between tags']) !!}
        </div>

        <div class="form-group">
            {!! Form::select('category_id' ,$categories,null , ['class'=>'form-control','placeholder'=>'Select Category']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Edit Post' , ['class'=>'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}


@stop