



@section('content')


    {!! Form::open(['method'=>'POST' , 'action'=>'Qowwa\Blog\CategoriesController@store' , 'files'=> true ]) !!}

            {{ csrf_field() }}

            <div class="form-group">
                {!! Form::text('title' ,null , ['class'=>'form-control','placeholder'=>'Category Title']) !!}
            </div>

            <div class="form-group">
                {!! Form::textarea('desc' ,null , ['class'=>'form-control','placeholder'=>'Category Description']) !!}
            </div>

            <div class="form-group">
                {!! Form::select('category_id' ,$categories,null , ['class'=>'form-control','placeholder'=>'Select Category']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('category_img' , 'Category Image:') !!}
                {!! Form::file('category_img' , ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Add New Category' , ['class'=>'btn btn-primary']) !!}
            </div>

        {!! Form::close() !!}


@stop