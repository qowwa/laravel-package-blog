<?php

namespace Qowwa\Blog;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['text'];


    public function posts()
    {
        return $this->belongsToMany('Qowwa\Blog\Post');
    }


}
