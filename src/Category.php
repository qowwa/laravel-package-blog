<?php

namespace Qowwa\Blog;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'title',
        'desc',
        'category_img',
        'category_id'
    ];

    public function category()
    {
        return $this->belongsTo('Qowwa\Blog\Category');
    }

    public function posts()
    {
        return $this->belongsToMany('Qowwa\Blog\Post');
    }

}
