<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Qowwa\Blog\Category;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('title','id')->all();
        return view('categories.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        if($file = $request->file('category_img'))
        {
            $name = time() . $file->getClientOriginalName();
            $file->move('uploads',$name);
            $inputs['category_img'] = $name;
        }
        Category::create($inputs);

        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::pluck('title','id');
        $category = Category::find($id);

        return view('categories.edit',compact('categories','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $category = Category::find($id);
        if($file = $request->file('category_img'))
        {
            if($category->category_img != null){
                unlink(public_path('uploads/'.$category->category_img));
            }
            $name = time() . $file->getClientOriginalName();
            $file->move('uploads',$name);
            $inputs['category_img'] = $name;
        }
        $category->update($inputs);
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if($category->category_img != null)
        {
            unlink(public_path('uploads/'.$category->category_img));
        }
        $category->delete();

        return redirect()->route('categories.index');
    }
}
