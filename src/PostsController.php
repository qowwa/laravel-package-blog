<?php

namespace Qowwa\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Qowwa\Blog\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        return view('posts.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::pluck('text','id')->all();
        $categories = Category::pluck('title','id')->all();
        return view('posts.create' ,compact('tags','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();
        $post->title = $request->title;
        $post->body = $request->body;
        if($file = $request->file('post_img'))
        {
            $name = time().$file->getClientOriginalName();
            $file->move('uploads',$name);
            $post->post_img = $name;
        }
        $post->user_id = Auth::guard()->user()->id;
        $post->save();
        $post->tags()->attach($request->tag_id);
        if($request->tag_name != '')
        {
            $tags = explode(',',$request->tag_name);
            foreach ($tags as $tag)
            {
                Tag::create(['text'=>$tag]);
            }
        }
        $post->categories()->attach($request->category_id);

        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $tags = Tag::pluck('text','id')->all();
        $categories = Category::pluck('title','id')->all();

        return view('posts.edit',compact('post','tags','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->user_id = Auth::guard()->user()->id;
        if($file = $request->file('post_img'))
        {
            if($post->post_img != null){
                unlink(public_path('uploads/'.$post->post_img));
            }
            $name = time().$file->getClientOriginalName();
            $file->move('uploads',$name);
            $post->post_img = $name;
        }
        if($request->tag_name != '')
        {
            $tags = explode(',',$request->tag_name);
            foreach ($tags as $tag)
            {
                Tag::create(['text'=>$tag]);
            }
        }
        $post->update();
        $post->tags()->sync($request->tag_id);
        $post->categories()->sync($request->category_id);

        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->tags()->detach();
        $post->categories()->detach();
        if($post->post_img != null){
            unlink(public_path('uploads/'.$post->post_img));
        }
        $post->delete();

        return redirect()->route('posts.index');
    }
}
