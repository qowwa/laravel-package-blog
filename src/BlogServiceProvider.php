<?php

namespace Qowwa\Blog;

use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';

        $this->publishes([
            __DIR__.'/migrations/' => database_path('migrations')], 'migrations');

        $this->publishes([
            __DIR__.'/views/' => resource_path('views')], 'views');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Qowwa\Blog\PostsController');
    }
}
