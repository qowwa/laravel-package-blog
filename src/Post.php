<?php

namespace Qowwa\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
      'title',
      'body',
       'post_img',
      'user_id'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tags()
    {
        return $this->belongsToMany('Qowwa\Blog\Tag');
    }

    public function categories()
    {
        return $this->belongsToMany('Qowwa\Blog\Category');
    }



}
